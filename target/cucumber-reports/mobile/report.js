$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/mobile/Login.feature");
formatter.feature({
  "name": "Mobile SportsBook Web Login Functionality",
  "description": "",
  "keyword": "Feature"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "user shares location and allows activity tracking and is on mobile sportsbook homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "MobileCommonSteps.userSharesLocationAndAllowsActivityTracking()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Open app",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@openApp"
    }
  ]
});
formatter.step({
  "name": "user opens app",
  "keyword": "When "
});
formatter.match({
  "location": "LoginSteps.userOpensApp()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});