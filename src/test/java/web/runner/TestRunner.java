package web.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/web"
        ,glue={"web/steps"}
        ,tags={"@login"}
        ,plugin = {"pretty","html:target/cucumber-reports/web"}
        ,dryRun = false
)
public class TestRunner {

}
