package web.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import web.pages.LoginPopUpPage;
import web.pages.PersonalAccountDropDown;
import web.pages.PostLoginHomePage;
import web.pages.PreLogInHomePage;
import web.utils.WaitUtils;

public class LoginSteps {

    WebDriver driver = Hooks.driver;
    PreLogInHomePage preLogInHomePage = new PreLogInHomePage(driver);
    LoginPopUpPage loginPopUpPage = new LoginPopUpPage(driver);
    PostLoginHomePage postloginHomePage = new PostLoginHomePage(driver);
    WebDriverWait wait = new WebDriverWait(Hooks.driver, 20);
    PersonalAccountDropDown personalAccountDropDown = new PersonalAccountDropDown(driver);
    JavascriptExecutor executor = (JavascriptExecutor) driver;
    WaitUtils waitUtils = new WaitUtils();


    @When("^user clicks \"([^\"]*)\" button on pre logged in homepage$")
    public void userClicksButton(String headerButton) throws Exception {
        switch (headerButton) {
            case "Login":
                wait.until(ExpectedConditions.visibilityOf(preLogInHomePage.loginButton));
                WaitUtils.waitForElementClickable(driver, preLogInHomePage.loginButton);
                preLogInHomePage.loginButton.click();
                Thread.sleep(2000);
                break;
            case "Register":
                WaitUtils.waitForElementClickable(driver, preLogInHomePage.registerButton);
                WaitUtils.waitForElementClickable(driver, preLogInHomePage.registerButton);
                preLogInHomePage.registerButton.click();
                Thread.sleep(2000);
                break;
            case "Home":
                WaitUtils.waitForElementClickable(driver, preLogInHomePage.homeButton);
                preLogInHomePage.homeButton.click();
                Thread.sleep(2000);
                break;
            case "Live":
                WaitUtils.waitForElementClickable(driver, preLogInHomePage.liveButton);
                preLogInHomePage.liveButton.click();
                Thread.sleep(2000);
                break;
            case "Casino":
                WaitUtils.waitForElementClickable(driver, preLogInHomePage.casinoButton);
                preLogInHomePage.casinoButton.click();
                Thread.sleep(2000);
                break;
            case "SportsBook Logo":
                WaitUtils.waitForElementClickable(driver, preLogInHomePage.sportsbookLogoButton);
                preLogInHomePage.sportsbookLogoButton.click();
                Thread.sleep(2000);
                break;
            default:
                throw new Exception("No button found!");
        }
    }

    @And("^user types \"([^\"]*)\" into \"([^\"]*)\" input field$")
    public void userTypesIntoInputField(String input, String inputField) throws Exception {
        switch (inputField) {
            case "email address":
            case "password":
                switch (input) {
                    case "incorrect email":
                        loginPopUpPage.emailAddressInputBox.sendKeys("testing@fubo.tv");
                        Thread.sleep(1000);
                        break;
                    case "incorrect password":
                        loginPopUpPage.passwordInputBox.sendKeys("Testing123456789123456!");
                        Thread.sleep(1000);
                        break;
                    case "correct email":
                        loginPopUpPage.emailAddressInputBox.sendKeys("dechols@fubo.tv");
                        Thread.sleep(1000);
                        break;
                    case "correct password":
                        loginPopUpPage.passwordInputBox.sendKeys("34Dwanna1!");
                        Thread.sleep(1000);
                        break;
                    default:
                        throw new Exception("No credentials where given");
                }
                break;
            default:
                throw new Exception("No input field found!");
        }
    }

    @And("^user clicks \"([^\"]*)\" button in login pop up$")
    public void userClicksButtonInLoginPopUp(String button) throws Exception {
        switch (button) {
            case "Login":
                loginPopUpPage.loginButton.click();
                break;
            default:
                throw new Exception("No button found!");
        }
    }

    @Then("^user should see and a \"([^\"]*)\" error message$")
    public void userShouldSeeAndErrorMessage(String errorMessage) throws Exception {
        switch (errorMessage) {
            case "That email and password combination is not valid.":
                Assert.assertEquals(errorMessage, loginPopUpPage.thatEmailAndPassWordCombinationIsNotValid.getText());
                Thread.sleep(1500);
                break;
            case "Are you sure that password is correct?":
                Assert.assertEquals(errorMessage, loginPopUpPage.areYouSureThantPasswordIsCorrect.getText());
                Thread.sleep(1500);
                break;
            default:
                throw new Exception("Error message not found!");
        }
    }

    @Then("^user should be logged into account$")
    public void userShouldBeLoggedIntoAccount() {
        WaitUtils.waitForElementVisible(driver, postloginHomePage.lastTimeBeingLoggedInElement);
        Assert.assertTrue(postloginHomePage.lastTimeBeingLoggedInElement.isDisplayed());
    }

    @And("^user clicks \"([^\"]*)\" on post logged in homepage$")
    public void userClicksInPostLoggedInHomepage(String headerButton) throws Exception {
        switch (headerButton) {
            case "personal user account button":
                Thread.sleep(2000);
                executor.executeScript("arguments[0].click();", postloginHomePage.personalUserAccountButton);
                Thread.sleep(2000);
                break;
            case "Deposit":
                executor.executeScript("arguments[0].click();", postloginHomePage.depositButton);
                Thread.sleep(2000);
                break;
            case "Live":
                Thread.sleep(2000);
                executor.executeScript("arguments[0].click();", postloginHomePage.liveButton);
                Thread.sleep(2000);
                break;
            default:
                throw new Exception("No element was found!");
        }
    }

    @And("^user clicks \"([^\"]*)\" button under personal account dropdown$")
    public void userClicksButtonUnderPersonalAccountDropdown(String button) throws Exception {
        switch (button) {
            case "Logout":
                executor.executeScript("arguments[0].click();", personalAccountDropDown.logoutButton);
                break;
            default:
                throw new Exception(("No button element was found!"));
        }
    }

    @Then("^user should be logged out of their account$")
    public void userShouldBeLoggedOutOfTheirAccount() {
        Assert.assertEquals("You have been logged out", driver.findElement(By.xpath("//*[@id=\"popupOverlay\"]/div/div[1]/h4")).getText());
    }
}
