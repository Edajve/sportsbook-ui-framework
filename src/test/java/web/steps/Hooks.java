package web.steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import web.utils.CommonUtils;
import web.utils.Driver;

public class Hooks {
    public static WebDriver driver;

    @Before
    public void setup() throws Exception {
        driver = Driver.getDriver();
    }
    @After
    public void tearDown(Scenario scenario) {
        CommonUtils.takeScreenshots(driver,scenario);
        driver.quit();
    }
}