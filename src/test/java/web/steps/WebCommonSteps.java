package web.steps;

import cucumber.api.java.en.Given;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import web.pages.LoginPopUpPage;
import web.pages.PreLogInHomePage;
import web.utils.ConfigReader;
import web.utils.WaitUtils;

public class WebCommonSteps {

    WebDriver driver = Hooks.driver;
    WebDriverWait wait = new WebDriverWait(Hooks.driver, 10);
    PreLogInHomePage preLogInHomePage = new PreLogInHomePage(Hooks.driver);
    LoginPopUpPage loginPopUpPage = new LoginPopUpPage(driver);

    @Given("^user goes to sportsbook homepage$")
    public void userGoesToSportsbookHomepage() throws Exception {
        switch (ConfigReader.getProperty("webEnvironment")) {
            case "local":
                Hooks.driver.get("http://localhost:9000/");
                break;
            case "ia":
                Hooks.driver.get("https://ia.fubosportsbook.com/en-us/live&geocomply=false");
                wait.until(ExpectedConditions.visibilityOf(preLogInHomePage.loginButton));
                break;
            case "dev5":
                Hooks.driver.get("https://fubo-dev5.amelcobetting.com/en-us/home&geocomply=false");
                wait.until(ExpectedConditions.visibilityOf(preLogInHomePage.loginButton));
                break;
            case "":
                break;
            default:
                throw new Exception("No environment has been selected!");
        }
    }

    @Given("^user logs into their account$")
    public void userLogsIntoTheirAccount() throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(preLogInHomePage.loginButton));
        WaitUtils.waitForElementClickable(driver, preLogInHomePage.loginButton);
        preLogInHomePage.loginButton.click();
        Thread.sleep(1000);
        loginPopUpPage.emailAddressInputBox.sendKeys("dechols@fubo.tv");
        Thread.sleep(1000);
        loginPopUpPage.passwordInputBox.sendKeys("34Dwanna1!");
        Thread.sleep(1000);
        loginPopUpPage.loginButton.click();
    }
}
