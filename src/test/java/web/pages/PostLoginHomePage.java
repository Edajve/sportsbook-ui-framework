package web.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PostLoginHomePage {

    public PostLoginHomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id=\"body-content\"]/div/div/div[2]/div/div/div[2]/div[3]/div[2]/span[1]")
    public WebElement lastTimeBeingLoggedInElement;

    @FindBy(xpath = "//*[@id=\"body-content\"]/div/div/div[2]/div/div/div[2]/div[3]/div[1]/div[1]/div")
    public WebElement personalUserAccountButton;

    @FindBy(css = "button[class='AccountAreaV2-cta-1']")
    public WebElement depositButton;

    @FindBy(xpath = "//*[@id=\"body-content\"]/div/div/div[2]/div/div/div[2]/div[2]/div/div[2]/ul/li[2]")
    public WebElement liveButton;
}
