package web.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPopUpPage {

    public LoginPopUpPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//div[@class='ce9028tm85qo15-innerWrapper-1 ce9028tm85qo15-innerWrapperRestrict-1 ce9028tm85qo15-innerWrapperOverflow-1']")
    public WebElement loginPopUpAlert;

    @FindBy(xpath = "//*[@id=\"username\"]")
    public WebElement emailAddressInputBox;

    @FindBy(xpath = "//*[@id=\"password\"]")
    public WebElement passwordInputBox;

    @FindBy(xpath = "/html/body/div[6]/div/div/div/div[2]/form/div/div/div/div[2]/button/span")
    public WebElement loginButton;

    @FindBy(xpath = "/html/body/div[6]/div/div/div/div[2]/div[2]/div/div[1]/div/div")
    public WebElement thatEmailAndPassWordCombinationIsNotValid;

    @FindBy(xpath = "/html/body/div[6]/div/div/div/div[2]/div[2]/div/div[1]/div/div")
    public WebElement areYouSureThantPasswordIsCorrect;
}
