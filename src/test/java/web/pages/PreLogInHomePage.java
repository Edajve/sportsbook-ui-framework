package web.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PreLogInHomePage {
    public PreLogInHomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    //Header
    @FindBy(id = "login-btn")
    public WebElement loginButton;

    @FindBy(xpath = "//*[@id=\"body-content\"]/div/div/div[2]/div/div/div[2]/div[3]/div/button")
    public WebElement registerButton;

    @FindBy(xpath = "//*[@id=\"body-content\"]/div/div/div[2]/div/div/div[2]/div[2]/div/div[2]/ul/li[1]")
    public WebElement homeButton;

    @FindBy(xpath = "//*[@id=\"body-content\"]/div/div/div[2]/div/div/div[2]/div[2]/div/div[2]/ul/li[2]")
    public WebElement liveButton;

    @FindBy(xpath = "//*[@id=\"body-content\"]/div/div/div[2]/div/div/div[2]/div[2]/div/div[2]/ul/li[3]")
    public WebElement casinoButton;

    @FindBy(xpath = "//div[@class='c7ljj7pfl0g22-logo-1']")
    public WebElement sportsbookLogoButton;

    //Footer

    //Left Panel Sports

    //Right Side bet slip panel
}
