package web.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PersonalAccountDropDown {

    public PersonalAccountDropDown(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id=\"body-content\"]/div[2]/div/div[2]/div/div/div[2]/div[3]/div[1]/div[1]/div[2]/div/div[8]/div/button")
    public WebElement logoutButton;

}
