package mobile.steps;


import cucumber.api.java.en.Given;
import mobile.utils.CommonUtils;
import org.openqa.selenium.By;
import web.utils.ConfigReader;

public class MobileCommonSteps {


    @Given("^user shares location and allows activity tracking and is on mobile sportsbook homepage$")
    public void userSharesLocationAndAllowsActivityTracking() throws InterruptedException {
        switch (ConfigReader.getProperty("device")) {
            case "android":
                CommonUtils.tapByElement(Hooks.androidDriver, Hooks.androidDriver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.Button[1]")));
                Thread.sleep(18000);
                break;
            case "ios":
                CommonUtils.tapByElement(Hooks.iosDriver, Hooks.iosDriver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Allow Once\"]\n")));
                CommonUtils.tapByElement(Hooks.iosDriver, Hooks.iosDriver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Allow\"]")));
                Thread.sleep(18000);
                break;
        }
    }
}
