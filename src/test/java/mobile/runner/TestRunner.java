package mobile.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/mobile"
        ,glue={"mobile/steps"}
        ,tags={"@openApp"}
        ,plugin = {"pretty","html:target/cucumber-reports/mobile"}
)
public class TestRunner {

}