@login
Feature: SportsBook Web Login Functionality

  Background:
    Given user goes to sportsbook homepage


  Scenario: Validate user can log in their account
    When user clicks "Login" button on pre logged in homepage
    And user types "correct email" into "email address" input field
    And user types "correct password" into "password" input field
    And user clicks "Login" button in login pop up
    Then user should be logged into account

  Scenario: Validate proper error message after incorrect credentials
    When user clicks "Login" button on pre logged in homepage
    And user types "incorrect email" into "email address" input field
    And user types "incorrect password" into "password" input field
    And user clicks "Login" button in login pop up
    Then user should see and a "That email and password combination is not valid." error message

  Scenario: Validate user should not be able to log in with no credentials
    When  user clicks "Login" button on pre logged in homepage
    And user clicks "Login" button in login pop up
    Then user should see and a "Are you sure that password is correct?" error message


  Scenario: Validate user should be able to log out of their account
    When user logs into their account
    Then user clicks "personal user account button" on post logged in homepage
    And user clicks "Logout" button under personal account dropdown
    Then user should be logged out of their account

    Scenario: Validate login button works
      When user clicks "Login" button on pre logged in homepage

